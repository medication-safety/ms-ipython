Unified network from "Interpretation of genomic variants using a unified biological network approach" (Khurana et al, PLoS Computational Biology, 2013) can be downloaded from here. 

Multinet.interactions.txt contains gene interaction pairs corresponding to the unified global network.

Multinet.interactions.network_presence.txt shows the presence of interactions in various constituent networks, where presence in a network is indicated by 1 while absence is indicated by 0. 
The last column shows the total number of networks that contain the interaction.

 
