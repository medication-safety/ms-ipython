To run the `Thousand genomes data.ipynb` notebook, please make sure miniconda (or a similar package manager) is installed, then:


**Create the environment:**

`conda env create -f environment.yml`


**Activate the environment:**

On Linux: `source activate pharmacogenomic_variation`

On Windows: `activate pharmacogenomic_variation`


**Run the notebook:**

`jupyter notebook`
