# http://www.r-tutor.com/elementary-statistics/inference-about-two-populations/comparison-two-population-proportions

# TODO: use allele counts or patient counts? using allele counts right now
GENES = c('CYP2C9', 'CYP2C19', 'CYP3A5', 'DPYD', 'SLCO1B1', 'TPMT', 'VKORC1')
ASSAYS = c('dmet_plus', 'florida_stanford_chip', 'taqman', 'veracode_adme_corepanel')

sig = 0.05
data = read.csv('output/superpopulation_counts.csv', sep="\t")

print("Gene, Assay, p-value of no difference between superpopulations")
for(gene in GENES){
  for(assay in ASSAYS){
    # get relevant data
    d = data[data$Gene == gene & data$Assay == assay, c("Superpopulation", "Problematic")]
    
    # make a table of counts for non-problematic (0) and problematic/no-call (1)
    tab = table(d$Superpopulation, factor(d$Problematic, lev=c(0, 1)))
    #print(tab)
    
    # do the test
    test = prop.test(tab)
    # ATTENTION: continuity correction does not get applied even if correct=T, see help page
    
    if(is.na(test$p.value)) {
      significant = "NOT SIGNIFICANT?"  # p-value is not available/not a number
      # this happens when all proportions are 0.0, but might happen in other cases as well, hence the ?...
    } else if (test$p.value < sig) {
      significant = "SIGNIFICANT"       # p-value is significant
    } else {
      significant = "NOT SIGNIFICANT"   # p-value is not significant
      
    }
    cat(sprintf("%-18s%-10s%-25s%.2e", significant, gene, assay, test$p.value), "\n")
  }
}

