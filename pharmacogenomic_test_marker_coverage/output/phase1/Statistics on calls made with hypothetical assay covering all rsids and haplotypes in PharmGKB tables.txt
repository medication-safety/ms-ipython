
**CYP2C19**

AFR
['*19'] : 19.92%
['*1B', '*26'] : 5.28%
['*1C'] : 0.41%
No-calls: 74.39%
N = 492

AMR
['*19'] : 58.01%
['*1B', '*26'] : 7.18%
No-calls: 34.81%
N = 362

ASN
['*19'] : 51.05%
['*1B', '*26'] : 2.80%
No-calls: 46.15%
N = 572

EUR
['*19'] : 47.76%
['*1B', '*26'] : 7.39%
['*2C'] : 1.06%
No-calls: 43.80%
N = 758

**CYP2C9**

AFR
['*1', '*17', '*19', '*20', '*21', '*22', '*23', '*26', '*27', '*28', '*29', '*30', '*32', '*34'] : 76.02%
['*11'] : 2.85%
['*2'] : 1.63%
['*31'] : 0.41%
['*5'] : 1.63%
['*8'] : 3.25%
['*9'] : 13.82%
No-calls: 0.41%
N = 492

AMR
['*1', '*17', '*19', '*20', '*21', '*22', '*23', '*26', '*27', '*28', '*29', '*30', '*32', '*34'] : 83.43%
['*12'] : 0.55%
['*2'] : 11.05%
['*5'] : 0.55%
['*8'] : 0.55%
No-calls: 3.87%
N = 362

ASN
['*1', '*17', '*19', '*20', '*21', '*22', '*23', '*26', '*27', '*28', '*29', '*30', '*32', '*34'] : 94.06%
['*13'] : 0.35%
['*2'] : 0.35%
No-calls: 5.24%
N = 572

EUR
['*1', '*17', '*19', '*20', '*21', '*22', '*23', '*26', '*27', '*28', '*29', '*30', '*32', '*34'] : 81.53%
['*12'] : 0.53%
['*2'] : 12.40%
['*8'] : 0.26%
No-calls: 5.28%
N = 758

**CYP2D6**

AFR
['*1', '*13', '*16', '*18', '*1A', '*1B', '*1C', '*1D', '*1E', '*1XN', '*22', '*23', '*24', '*25', '*26', '*27', '*27XN', '*38', '*44', '*48', '*48XN', '*50', '*60', '*61', '*62', '*71', '*75'] : 24.80%
['*10', '*10A', '*10B', '*10D', '*36', '*36 Dupl/Tandem', '*36 Single', '*37', '*47', '*54', '*56B', '*57', '*72'] : 3.66%
['*11', '*19', '*2', '*20', '*21', '*21A', '*21B', '*2A', '*2B', '*2C', '*2D', '*2E', '*2F', '*2G', '*2H', '*2K', '*2L', '*2M', '*2XN', '*30', '*31', '*32', '*42', '*51', '*55', '*56', '*56A', '*59', '*73'] : 17.48%
['*17', '*40', '*58'] : 22.36%
['*29'] : 9.35%
['*3', '*3A'] : 0.41%
['*35', '*35A', '*35B', '*35X2'] : 0.41%
['*4', '*4A', '*4B', '*4F', '*4G', '*4H', '*4N'] : 2.03%
['*41'] : 1.63%
['*43'] : 2.44%
['*45', '*45A', '*45B', '*45XN'] : 4.88%
['*46', '*46XN'] : 0.81%
['*4C', '*4D', '*4E', '*4L'] : 6.91%
['*6', '*6A', '*6D'] : 0.41%
['*70'] : 0.41%
No-calls: 2.03%
N = 492

AMR
['*1', '*13', '*16', '*18', '*1A', '*1B', '*1C', '*1D', '*1E', '*1XN', '*22', '*23', '*24', '*25', '*26', '*27', '*27XN', '*38', '*44', '*48', '*48XN', '*50', '*60', '*61', '*62', '*71', '*75'] : 49.72%
['*11', '*19', '*2', '*20', '*21', '*21A', '*21B', '*2A', '*2B', '*2C', '*2D', '*2E', '*2F', '*2G', '*2H', '*2K', '*2L', '*2M', '*2XN', '*30', '*31', '*32', '*42', '*51', '*55', '*56', '*56A', '*59', '*73'] : 16.02%
['*17', '*40', '*58'] : 0.55%
['*29'] : 0.55%
['*3', '*3A'] : 0.55%
['*35', '*35A', '*35B', '*35X2'] : 2.76%
['*39', '*39XN'] : 0.55%
['*4', '*4A', '*4B', '*4F', '*4G', '*4H', '*4N'] : 10.50%
['*41'] : 6.63%
['*43'] : 0.55%
['*4C', '*4D', '*4E', '*4L'] : 3.87%
['*6', '*6A', '*6D'] : 1.10%
['*65'] : 0.55%
['*9'] : 2.76%
No-calls: 3.31%
N = 362

ASN
['*1', '*13', '*16', '*18', '*1A', '*1B', '*1C', '*1D', '*1E', '*1XN', '*22', '*23', '*24', '*25', '*26', '*27', '*27XN', '*38', '*44', '*48', '*48XN', '*50', '*60', '*61', '*62', '*71', '*75'] : 34.27%
['*10', '*10A', '*10B', '*10D', '*36', '*36 Dupl/Tandem', '*36 Single', '*37', '*47', '*54', '*56B', '*57', '*72'] : 49.65%
['*11', '*19', '*2', '*20', '*21', '*21A', '*21B', '*2A', '*2B', '*2C', '*2D', '*2E', '*2F', '*2G', '*2H', '*2K', '*2L', '*2M', '*2XN', '*30', '*31', '*32', '*42', '*51', '*55', '*56', '*56A', '*59', '*73'] : 10.14%
['*35', '*35A', '*35B', '*35X2'] : 0.35%
['*39', '*39XN'] : 1.05%
['*41'] : 2.80%
['*4C', '*4D', '*4E', '*4L'] : 0.35%
['*65'] : 0.70%
No-calls: 0.70%
N = 572

EUR
['*1', '*13', '*16', '*18', '*1A', '*1B', '*1C', '*1D', '*1E', '*1XN', '*22', '*23', '*24', '*25', '*26', '*27', '*27XN', '*38', '*44', '*48', '*48XN', '*50', '*60', '*61', '*62', '*71', '*75'] : 38.79%
['*10', '*10A', '*10B', '*10D', '*36', '*36 Dupl/Tandem', '*36 Single', '*37', '*47', '*54', '*56B', '*57', '*72'] : 1.32%
['*11', '*19', '*2', '*20', '*21', '*21A', '*21B', '*2A', '*2B', '*2C', '*2D', '*2E', '*2F', '*2G', '*2H', '*2K', '*2L', '*2M', '*2XN', '*30', '*31', '*32', '*42', '*51', '*55', '*56', '*56A', '*59', '*73'] : 22.43%
['*3', '*3A'] : 1.32%
['*33', '*33XN'] : 0.53%
['*35', '*35A', '*35B', '*35X2'] : 4.22%
['*39', '*39XN'] : 0.26%
['*4', '*4A', '*4B', '*4F', '*4G', '*4H', '*4N'] : 13.46%
['*41'] : 8.71%
['*4C', '*4D', '*4E', '*4L'] : 5.28%
['*6', '*6A', '*6D'] : 1.58%
['*9'] : 1.32%
No-calls: 0.79%
N = 758

**CYP3A5**

AFR
['*1', '*1B', '*1C', '*1D', '*1E', '*5'] : 53.66%
['*3', '*3C', '*3E', '*3G', '*3H', '*3I', '*3J'] : 19.51%
['*6'] : 15.85%
['*7'] : 10.57%
No-calls: 0.41%
N = 492

AMR
['*1', '*1B', '*1C', '*1D', '*1E', '*5'] : 19.89%
['*3', '*3C', '*3E', '*3G', '*3H', '*3I', '*3J'] : 76.24%
['*3B'] : 1.66%
['*6'] : 2.21%
No-calls: 0.00%
N = 362

ASN
['*1', '*1B', '*1C', '*1D', '*1E', '*5'] : 27.27%
['*3', '*3C', '*3E', '*3G', '*3H', '*3I', '*3J'] : 71.33%
['*3F'] : 1.40%
No-calls: 0.00%
N = 572

EUR
['*1', '*1B', '*1C', '*1D', '*1E', '*5'] : 4.49%
['*3', '*3C', '*3E', '*3G', '*3H', '*3I', '*3J'] : 93.40%
['*3B'] : 1.58%
No-calls: 0.53%
N = 758

**DPYD**

AFR
['*1'] : 43.50%
['*4'] : 0.81%
['*5'] : 9.35%
['*6'] : 0.81%
['*9A'] : 33.33%
No-calls: 12.20%
N = 492

AMR
['*1'] : 53.04%
['*4'] : 1.66%
['*5'] : 12.15%
['*6'] : 8.84%
['*9A'] : 19.34%
No-calls: 4.97%
N = 362

ASN
['*1'] : 65.73%
['*5'] : 28.67%
['*6'] : 1.05%
['*9A'] : 4.20%
No-calls: 0.35%
N = 572

EUR
['*1'] : 56.46%
['*2A'] : 0.53%
['*2B'] : 0.26%
['*4'] : 2.64%
['*5'] : 15.04%
['*6'] : 2.11%
['*9A'] : 19.26%
No-calls: 3.69%
N = 758

**SLCO1B1**

AFR
['*1', '*1A', '*1C', '*2', '*36'] : 11.38%
['*19'] : 0.41%
['*1B'] : 18.70%
['*20'] : 5.69%
['*21'] : 36.18%
['*22'] : 0.41%
['*24'] : 0.41%
['*31'] : 5.28%
['*35'] : 1.22%
No-calls: 20.33%
N = 492

AMR
['*1', '*1A', '*1C', '*2', '*36'] : 10.50%
['*1B'] : 14.92%
['*20'] : 4.97%
['*21'] : 7.18%
['*35'] : 0.55%
No-calls: 61.88%
N = 362

ASN
['*1B'] : 28.67%
['*21'] : 31.47%
No-calls: 39.86%
N = 572

EUR
['*1', '*1A', '*1C', '*2', '*36'] : 11.35%
['*19'] : 0.26%
['*1B'] : 2.37%
['*20'] : 2.64%
['*21'] : 5.28%
['*35'] : 0.26%
No-calls: 77.84%
N = 758

**TPMT**

AFR
['*1', '*10', '*11', '*12', '*17', '*18', '*19', '*1A', '*2', '*22', '*25', '*27', '*28', '*30', '*35', '*36', '*3A', '*3B', '*3C', '*3D', '*4', '*5', '*6', '*7', '*8', '*9'] : 18.70%
['*1S'] : 5.69%
['*3E'] : 0.81%
No-calls: 74.80%
N = 492

AMR
['*1', '*10', '*11', '*12', '*17', '*18', '*19', '*1A', '*2', '*22', '*25', '*27', '*28', '*30', '*35', '*36', '*3A', '*3B', '*3C', '*3D', '*4', '*5', '*6', '*7', '*8', '*9'] : 31.49%
['*1S'] : 9.39%
No-calls: 59.12%
N = 362

ASN
['*1', '*10', '*11', '*12', '*17', '*18', '*19', '*1A', '*2', '*22', '*25', '*27', '*28', '*30', '*35', '*36', '*3A', '*3B', '*3C', '*3D', '*4', '*5', '*6', '*7', '*8', '*9'] : 51.05%
['*1S'] : 12.94%
No-calls: 36.01%
N = 572

EUR
['*1', '*10', '*11', '*12', '*17', '*18', '*19', '*1A', '*2', '*22', '*25', '*27', '*28', '*30', '*35', '*36', '*3A', '*3B', '*3C', '*3D', '*4', '*5', '*6', '*7', '*8', '*9'] : 41.69%
['*1S'] : 6.33%
No-calls: 51.98%
N = 758

**VKORC1**

AFR
['*1', 'H3'] : 14.23%
['*1', 'H6'] : 6.10%
['*1'] : 7.32%
['*2', 'H1'] : 5.69%
['*2', 'H2'] : 1.22%
['*3', 'H7'] : 41.06%
['*3', 'H8'] : 1.63%
['*3'] : 3.25%
['*4', 'H9'] : 4.47%
['*4'] : 0.41%
['H4'] : 13.82%
No-calls: 0.81%
N = 492

AMR
['*1', 'H3'] : 3.87%
['*1'] : 1.10%
['*2', 'H1'] : 22.10%
['*2', 'H2'] : 14.92%
['*2', 'H5'] : 4.42%
['*3', 'H7'] : 32.04%
['*3', 'H8'] : 4.97%
['*4', 'H9'] : 16.02%
['H4'] : 0.55%
No-calls: 0.00%
N = 362

ASN
['*2', 'H1'] : 90.56%
['*2', 'H2'] : 0.35%
['*3', 'H7'] : 9.09%
No-calls: 0.00%
N = 572

EUR
['*1', 'H3'] : 1.58%
['*1', 'H6'] : 0.53%
['*2', 'H1'] : 16.62%
['*2', 'H2'] : 21.90%
['*2', 'H5'] : 1.06%
['*3', 'H7'] : 27.44%
['*3', 'H8'] : 10.29%
['*3'] : 0.26%
['*4', 'H9'] : 14.78%
['*4'] : 5.54%
No-calls: 0.00%
N = 758
