�cpyleaf.rrc
resource
q )�q}q(X   _fingerprintqX�  def plots(regression_o, getData_o):
    """Plots the dataset with a regression line and a boxplot using R."""
    fname1 = 'car_regress.pdf'
    r.pdf(fname1)
    r.plot(getData_o, ylab='dist', xlab='speed')
    r.abline(regression_o['(Intercept)'], regression_o['y'], col='red')
    r.dev_off()

    fname2 = 'car_hist.pdf'
    r.pdf(fname2)
    r.boxplot(getData_o, names=['dist', 'speed'])
    r.dev_off()

    return fname1, fname2
qX   _nameqX   plotsqX	   _contentsqcex2
plots
qX   _pathq	X   leaf_ex2/plots.modq
ub.